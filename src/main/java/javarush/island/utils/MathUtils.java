package javarush.island.utils;

import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@UtilityClass
//Вспомогательный класс. @UtilityClass делает все методы статическими и создаёт конструктор с ошибкой инициализации
//Поскольку вспомогательный класс не может быть инициализирован
public class MathUtils {

    //Метод для заключения значения. Допустим clamp(5, 0, 10) => 5
    //clamp(-1, 0, 10) => 0; clamp(100, 0, 10) => 10
    //Думаю суть ясна, так работает не только с целыми числами, т.к. метод обобщён с помощью T
    public <T extends Comparable<T>> T clamp(T val, T min, T max) {
        if (val.compareTo(min) < 0) return min;
        else if (val.compareTo(max) > 0) return max;
        else return val;
    }

    //Метод для рандомной выборки из списка
    public <T> T pickRandomFromList(List<T> inputList) {
        if(inputList.size() == 0) return null;

        return inputList.get(
                ThreadLocalRandom.current().nextInt(0, inputList.size())
        );
    }

}
