package javarush.island.utils;

import javarush.island.datatypes.Location;
import javarush.island.items.BasicItem;
import javarush.island.items.Plant;
import lombok.experimental.UtilityClass;

import java.util.*;

@UtilityClass
//Вспомогательный класс визуализатор. Логика класса такая же, как и MathUtils
//Создан для изображения текущего состояния острова
public class Visualizer {

    /**
     * Метод для визуализации массива локаций.
     * @param locations Массив локаций острова
     * @param maxWidth Максимальная ширина острова
     * @param maxHeight Максимальная высота острова
     */
    public void visualiseLocationsArray(Location[][] locations, int maxWidth, int maxHeight) {
        System.out.println("Visualizing: X:Y   animals");

        StringBuilder stringBuilder = new StringBuilder();
        for (int x = 0; x < maxWidth; x++) {
            for (int y = 0; y < maxHeight; y++) {
                Location location = locations[x][y];

                //Визуализируем: Эмодзи + количество
                Map<String, Double> visualize = new HashMap<>();
                location.getLocationItems(null)
                        .forEach(basicItem -> {
                            String emoji = basicItem.getEmoji();
                            visualize.put(emoji, visualize.getOrDefault(emoji, 0.0) + visualizeBasicItemCount(basicItem));
                        });

                stringBuilder.append(x).append(":").append(y)
                        .append("\t").append(mapToString(visualize)).append("\n");
            }
        }

        System.out.print("\n" + stringBuilder);

    }

    /**
     * Метод, визуализирующий количество BasicItem`а
     * Если это растение - берём его вес, иначе 1
     * @param basicItem BasicItem
     * @return Количество BasicItem`а
     */
    private double visualizeBasicItemCount(BasicItem basicItem) {
        if(Plant.class.isAssignableFrom(basicItem.getClass())) {
            return ((Plant) basicItem).getWeight();
        }

        return 1;
    }

    /**
     * Визуализирует мапу в строку
     * @param inputMap Input map
     * @return строка типа @keyx@value
     */
    private StringBuilder mapToString(Map<String, Double> inputMap) {
        StringBuilder builder = new StringBuilder();

        for(String key : inputMap.keySet()) {
            builder.append(key).append("x").append(inputMap.get(key).intValue()).append("   ");
        }

        return builder;
    }

}
