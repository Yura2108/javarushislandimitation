package javarush.island.items;

import javarush.island.Island;
import javarush.island.config.Configuration;
import javarush.island.datatypes.CreatureType;
import javarush.island.datatypes.Direction;
import javarush.island.datatypes.ITicking;
import javarush.island.datatypes.Location;
import javarush.island.statistic.ImitationStatistic;
import javarush.island.utils.MathUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@Setter
@ToString
public abstract class Animal extends EdibleItem implements ITicking, Cloneable {
    /**
     * Абстрактный класс для любого животного.
     * Наследует EdibleItem, поскольку может быть съеден (имеет свой вес).
     * Наследует ITicking, т.к. обновляется каждый шаг симуляции.
     * Наследует Cloneable чтобы можно было клонировать
     */


    //Настройки животного.
    private final int speed;
    private final double howMuchCanEat;

    //Map with fractional chances of eat other creatures
    private final Map<CreatureType, Double> probabilityEatCreatures;

    //Насыщенность (голод) животного
    private double foodSaturation;

    //Специально высчитанный параметр, на сколько голод уменьшается каждый ход симуляции
    private final double saturationReductionRate;

    //Временное направление движения
    private Direction direction = null;

    public Animal(int maxQuantityInCell, int x, int y, String emoji, double weight, int speed,
                  double howMuchCanEat, Map<CreatureType, Double> probabilityEatCreatures) {
        super(maxQuantityInCell, x, y, emoji, weight);

        this.speed = speed;
        this.howMuchCanEat = howMuchCanEat;
        this.probabilityEatCreatures = probabilityEatCreatures;

        this.foodSaturation = getWeight();
        this.saturationReductionRate = howMuchCanEat / getWeight();
    }

    /**
     * @param edibleItem Что можно съесть
     * @return Вернёт десятичный шанс съесть другую сущность (Десятичный шанс: 0 <= chance <= 1)
     */
    public double getProbabilityEatAnother(EdibleItem edibleItem) {
        CreatureType creatureType = edibleItem.getCreatureType();

        if(creatureType == null) return 0;

        return probabilityEatCreatures.getOrDefault(creatureType, 0.0);
    }

    @Override
    public void doSimulationTick() {
        //Каждый тик симуляции вызывается данный метод

        //Получаем локацию, на которой стоит животное, на данный момент
        Location currentLocation = Island.getCurrentSimulation().getLocationOnCoordinates(this.getX(), this.getY());

        //Если попали в шанс - кушаем
        if(ThreadLocalRandom.current().nextBoolean()) eat(currentLocation);

        //После отнимаем голод и проверяем, не упал ли он ниже нуля
        this.foodSaturation -= saturationReductionRate;
        if(foodSaturation <= 0) {
            //Если голод <= 0  -> сущность умерла, удаляем с текущей локации
            this.setWeight(-1);
            currentLocation.removeItem(this);
            return;
        }

        //Если животное живо - добавим количество в статистику
        Island.getCurrentSimulation().addStatistic(ImitationStatistic.EventType.ANIMALS_COUNT);

        //Если мы попали в шанс и на локации есть свободное место - можем родить потомство
        if(ThreadLocalRandom.current().nextBoolean() &&
                currentLocation.getCountOfCreatures(getCreatureType()) < getMaxQuantityInCell())
            reproduce(currentLocation);

        //Если мы попали в шанс - можем попробовать двигаться
        if(ThreadLocalRandom.current().nextBoolean()) move(currentLocation);
    }

    /**
     * Попытка съесть кого-то/что-то
     * @param currentLocation Текущая позиция животного
     */
    public void eat(Location currentLocation) {
        //Выбираем из текущей локации жертву
        BasicItem basicItem = MathUtils.pickRandomFromList(currentLocation.getLocationItems(this));
        if(basicItem == null) return;

        //Если жертву нельзя съесть - не повезло
        if(!EdibleItem.class.isAssignableFrom(basicItem.getClass())) return;

        EdibleItem edibleItem = (EdibleItem) basicItem;

        //Проверяем, является ли растением наша цель
        boolean victimPlant = Plant.class.isAssignableFrom(basicItem.getClass());
        if(!victimPlant && edibleItem.getWeight() > howMuchCanEat) return;

        //Повезло ли нам съесть нашу жертву
        if(Math.random() > getProbabilityEatAnother(edibleItem)) return;

        //Пополняем наш голод + отнимаем у жертвы вес (если это растение)
        double requiredSaturation = getWeight() - foodSaturation;
        if(victimPlant && edibleItem.getWeight() > requiredSaturation) {
            this.updateFoodSaturationFactor(requiredSaturation);

            edibleItem.setWeight(edibleItem.getWeight() - requiredSaturation);
        }else {
            //Если это не растение, а животное - умрёт сразу
            this.updateFoodSaturationFactor(edibleItem.getWeight());

            edibleItem.setWeight(-1);
        }

        //Если жертва умерла - удаляем с локации
        if(edibleItem.isDeath()) currentLocation.removeItem(edibleItem);

        //Обновляем значение статистики
        Island.getCurrentSimulation().addStatistic(ImitationStatistic.EventType.EAT);
    }

    /**
     * Метод для обновления голода
     * @param victimWeight Сколько нужно голода добавить
     */
    private void updateFoodSaturationFactor(double victimWeight) {
        if(foodSaturation + victimWeight > getWeight()) foodSaturation = getWeight();
        else foodSaturation += victimWeight;
    }

    /**
     * Метод для движения.
     * @param currentLocation Текущая позиция животного
     */
    public void move(Location currentLocation) {
        //Выбираем направление для движения
        this.direction = Direction.getRandomNonEmpty();

        //Двигаемся
        moveInCurrentDirection(currentLocation);
    }

    /**
     * @param currentLocation Текущая позиция животного
     */
    private void moveInCurrentDirection(Location currentLocation) {
        if(direction == null) {
            throw new NullPointerException("Trying to move in null direction!");
        }

        if(direction.equals(Direction.EMPTY)) return;

        //Получаем текущие координаты
        int resultX = getX(), resultY = getY();

        //Просчитываем координаты, как результат движения
        switch (direction) {
            case UP: {
                resultY = MathUtils.clamp(getY() + speed, 0, Island.getCurrentSimulation().getClampHeight());
                break;
            }
            case DOWN: {
                resultY = MathUtils.clamp(getY() - speed, 0, Island.getCurrentSimulation().getClampHeight());
                break;
            }

            case RIGHT: {
                resultX = MathUtils.clamp(getX() + speed, 0, Island.getCurrentSimulation().getClampWidth());
                break;
            }

            case LEFT: {
                resultX = MathUtils.clamp(getX() - speed, 0, Island.getCurrentSimulation().getClampWidth());
                break;
            }

            default:
                return;
        }

        //Получаем локацию на полученных координатах
        Location resultLocation = Island.getCurrentSimulation().getLocationOnCoordinates(resultX, resultY);

        //Если как-то получилась та же локация - уходим
        if(resultLocation.equals(currentLocation)) return;

        //Попытка передвинуть наше животное в другую клетку
        boolean successful = resultLocation.addNewBasicItem(this);

        //Если что-то не вышло (нет места) - уходим
        if(!successful) return;

        //Move allowed
        this.setX(resultX);
        this.setY(resultY);

        //Удаляем животное из текущей локации
        currentLocation.removeItem(this);

        //Добавляем статистику
        Island.getCurrentSimulation().addStatistic(ImitationStatistic.EventType.MOVE);
    }

    /**
     * Создаём потомство
     * @param currentLocation Текущая позиция животного
     */
    public void reproduce(Location currentLocation) {
        //клонируем наше животное
        Animal child = this.clone();

        //Сбрасываем голод и направление движения
        child.setFoodSaturation(child.getWeight());
        child.setDirection(null);

        //Добавляем на локацию животное
        currentLocation.addChild(child);

        //Добавляем значение в статистику
        Island.getCurrentSimulation().addStatistic(ImitationStatistic.EventType.REPRODUCE);
    }


    /**
     * Creates a new animal (Class specified by enum). With reflection utils and config settings
     * @param creatureType Type of creature
     * @param configuration Configuration instance (To prevent permanent calls to .getConfiguration())
     * @return New Animal with config settings
     */
    public static Animal createNewAnimal(CreatureType creatureType, Configuration configuration,
                                         int xPosition, int yPosition) {
        try {
            //С помощью рефлексии получаем конструктор для создания животного указанного класса
            Constructor<? extends BasicItem> constructor =
                    creatureType.getItemClass().getDeclaredConstructor(Animal.class.getConstructors()[0].getParameterTypes());

            //Создаём новое животное
            return (Animal) constructor.newInstance(
                    configuration.getCreatureSetting(creatureType, "maxNumberPerOneLocation", Number.class).intValue(),
                    xPosition, yPosition,
                    configuration.getCreatureSetting(creatureType, "emoji", String.class),
                    configuration.getCreatureSetting(creatureType, "weight", Number.class).doubleValue(),
                    configuration.getCreatureSetting(creatureType, "movementSpeed", Number.class).intValue(),
                    configuration.getCreatureSetting(creatureType, "weightFoodCanEat", Number.class).doubleValue(),
                    configuration.getProbabilityEatCreatures(creatureType)
            );
        }catch (NoSuchMethodException exception) {
            System.err.println("Trying to create invalid animal ".concat(creatureType.name()));
        }catch (IllegalArgumentException exception) {
            System.err.println("Trying to create animal with invalid arguments ".concat(creatureType.name()));
        }catch (Exception exception) {
            exception.printStackTrace();

            System.err.println("Exception when use reflection for: ".concat(creatureType.name()));
        }

        return null;
    }

    @Override
    public Animal clone() {
        try {
            return (Animal) super.clone();
        }catch (CloneNotSupportedException exception) {
            exception.printStackTrace();

            return null;
        }
    }

    // --- DEBUG METHODS ---

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;

        return getX() == animal.getX() && getY() == animal.getY();
    }
}