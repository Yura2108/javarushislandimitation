package javarush.island;

import javarush.island.config.Configuration;
import javarush.island.datatypes.Location;
import javarush.island.datatypes.SimulationEndReason;
import javarush.island.statistic.ImitationStatistic;
import javarush.island.utils.Visualizer;
import lombok.Getter;

//Основной класс симуляции, где хранятся настройки, локации и вся необходимая информация
public class Island {

    //На будущее, аннотация @Getter - создаёт метод-геттер для поля, в данном случае будет сгенерирован метод
    //public static Island getCurrentSimulation();
    //После компиляции программы аннотации уходят, превращаются в код, который они обозначали
    @Getter
    //Тут мы храним нашу симуляцию, чтобы к ней можно было обратиться
    private static Island currentSimulation;

    @Getter
    //тут мы храним массив с локациями (наши клетки)
    private final Location[][] locations;

    //Тут храним статистику на каждый тик симуляции
    private final ImitationStatistic statistic;

    //Основные настройки острова
    private final int width, height, statisticsUpdateFrequency;

    private int turnsCount;

    //Тут мы храним причину, по которой наша симуляция закончилась, естественно, изначально она null
    //В процессе работы может изменится
    private SimulationEndReason simulationEndReason = null;

    public Island(String settingsFilePath) {
        //Инициализируем конфиг. Туда же передаём полученный путь до файла настроек (или null, если не указан)
        Configuration configuration = Configuration.getInstance(settingsFilePath);

        //Инициализируем статистику
        statistic = new ImitationStatistic();

        //Получаем из конфига ширину и высоту острова
        width = configuration.getIslandSetting("width", Integer.class);
        height = configuration.getIslandSetting("height", Integer.class);

        //Получаем из конфига частоту обновления статистики
        statisticsUpdateFrequency = configuration.getIslandSetting("statisticsUpdateFrequency", Integer.class);

        //Получаем из конфига максимальное количество шагов имитации
        turnsCount = configuration.getIslandSetting("turnsCount", Integer.class);

        //Получаем кастомный параметр - шанс появится какой-либо сущности на локации
        double creatureSpawnChance = configuration.getIslandSetting("creatureSpawnChance", Number.class).doubleValue();

        //Инициализируем массив локаций
        locations = new Location[width][height];

        //Инициализируем каждую локацию
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //Создаём локацию на текущих координатах, передаём туда настройки, координаты, шанс на появление сущностей
                Location location = new Location(configuration, i, j, creatureSpawnChance);

                //Запускаем инициализацию локации
                location.initializeLocation();

                //Устанавливаем локацию в массив
                locations[i][j] = location;
            }
        }
    }

    //Основной цикл симуляции
    private void startSimulationLoop() {
        //Цикл будет работать, пока причина окончания симуляции не будет установлена
        while (simulationEndReason == null) {
            //Если количество ходов закончилось - останавливаем симуляцию
            if(turnsCount <= 0) {
                simulationEndReason = SimulationEndReason.MOVES_ARE_OVER;

                continue;
            }

            //Выполняем основной ход симуляции - проходимся по всем локациям и вызываем соответствующий метод
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    locations[i][j].doSimulationTick();
                }
            }

            //Если количество животных ноль - остановим симуляцию
            if(statistic.getStatisticValue(ImitationStatistic.EventType.ANIMALS_COUNT) <= 0) {
                simulationEndReason = SimulationEndReason.ALL_ANIMALS_DIED;
                continue;
            }

            //Вызываем класс визуализатора и отображаем клетки на текущем такте
            Visualizer.visualiseLocationsArray(locations, width, height);
            System.out.println("\n");

            //Выводим статистику в консоль
            statistic.printStatisticAndClear();

            //Убавляем количество ходов
            turnsCount--;

            //Кидаем в слип основной поток, чтобы отображение работало +- плавно и читабельно
            try {
                Thread.sleep(statisticsUpdateFrequency);
            }catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }

        //Если вышли из цикла - значит была установлена причина остановки симуляции, завершаем её
        endSimulation();
    }

    private void endSimulation() {
        //Выводим в консоль информацию об окончании
        System.out.println("\n\n Simulation ended: " + simulationEndReason.getShortDescription() + "\n\n");

        //Убираем инстанс текущей симуляции
        currentSimulation = null;
    }

    //Метод надстройка, для добавления статистики
    public void addStatistic(ImitationStatistic.EventType eventType) {
        statistic.addStatistic(eventType);
    }

    //Методы геттеры для высоты и ширины, отнимают 1, используются для циклов
    public int getClampHeight() {
        return height - 1;
    }

    public int getClampWidth() {
        return width - 1;
    }

    public Location getLocationOnCoordinates(int x, int y) {
        //Реализация инкапсуляции (ООП). Проверка на валидность переданных аргументов
        if ((x >= width || x < 0) || (y >= height || y < 0)) {
            throw new IllegalArgumentException("Invalid X or Y values passed " + x + " " + y);
        }

        //Возвращаем нужную локацию
        return locations[x][y];
    }

    public static void startSimulation(String settingsFilePath) {
        //Реализация паттерна проектирования Singleton. У симуляции может быть только один текущий instance.
        if(currentSimulation != null) {
            throw new IllegalStateException("Simulation already started!");
        }

        //Запуск симуляции
        currentSimulation = new Island(settingsFilePath);

        currentSimulation.startSimulationLoop();
    }
}
