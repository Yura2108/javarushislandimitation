package javarush.island.config;

import javarush.island.datatypes.CreatureType;
import lombok.Getter;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

@Getter
public class Configuration {
    /**
     * Класс описывающий конфиг и созданный для взаимодействия с ним, хранит все настройки
     */

    private static Configuration INSTANCE;

    //Настройки сущностей
    private final Map<String, Object> creaturesSettings = new HashMap<>();

    //Настройки острова
    private final Map<String, Object> islandSettings = new HashMap<>();

    @SuppressWarnings("unchecked")
    private Configuration(String settingsFilePath) {
        //Загружаем Yml файл настроек
        Yaml yaml = new Yaml();

        //По итогу загрузки файла мы получим такую мапу
        Map<String, Object> allSettings = null;
        try {
            if(settingsFilePath == null) {
                //Если используем стандартный файл настроек
                //Подгружаем ресурсы (java/resources)
                allSettings = yaml.load(getClass().getClassLoader().getResourceAsStream("creatures.yml"));
            }else {
                File file = new File(settingsFilePath);

                allSettings = yaml.load(
                        new FileInputStream(file)
                );
            }
        } catch (Exception exception) {
            exception.printStackTrace();

            System.err.println("File with creatures settings does not found!");
            System.exit(1);
        }

        //Если мапа так и осталась null
        if(allSettings == null) {
            throw new NullPointerException("Settings not loaded");
        }

        //Если в настройках нету нужных нам
        if(!allSettings.containsKey("IslandSettings") || !allSettings.containsKey("Creatures")) {
            System.err.println("Main settings does not found in settings-file!");
            System.exit(1);
        }

        //Распределяем наши настройки
        islandSettings.putAll(((Map<String, Object>) allSettings.get("IslandSettings")));
        creaturesSettings.putAll(((Map<String, Object>) allSettings.get("Creatures")));
    }

    //Singleton
    public static Configuration getInstance(String settingsFilePath) {
        if (INSTANCE == null) {
            INSTANCE = new Configuration(settingsFilePath);
        }
        return INSTANCE;
    }

    //Обобщённый метод для получения настроек острова с возможностью скастить сразу
    //Вместо Double d = (double) getSetting('123')
    //Используем double d = getSetting('123', Double.class)
    //По итогу получаем сразу нужный нам класс
    public <T> T getIslandSetting(String settingName, Class<T> castTo) {
        Object setting = islandSettings.getOrDefault(settingName, null);

        if(setting == null) {
            throw new NullPointerException("Parameter named ".concat(settingName).concat(" not found"));
        }

        return applyGeneric(setting, castTo);
    }

    @SuppressWarnings("unchecked")
    //Такой же метод, как и getIslandSetting, только связан с настройками сущностей
    public <T> T getCreatureSetting(CreatureType creatureType, String settingName, Class<T> castTo) {
        Map<String, Object> creatureSettings = (Map<String, Object>) creaturesSettings.get(creatureType.getSettingsPath());

        Object setting = creatureSettings.getOrDefault(settingName, null);

        if(setting == null) {
            throw new NullPointerException("Parameter named ".concat(settingName).concat(" not found").concat(" ")
                    .concat(creatureType.name()));
        }

        return applyGeneric(setting, castTo);
    }

    //Обобщённый метод для превращения Object в <T>
    private <T> T applyGeneric(Object setting, Class<T> castTo) {
        if(!castTo.isAssignableFrom(setting.getClass())) {
            throw new IllegalArgumentException("Trying to cast to the wrong type: "
                    .concat(setting.getClass().getName())
                    .concat(" => ")
                    .concat(castTo.getName())
            );
        }

        return castTo.cast(setting);
    }

    //Метод для получения шансов на съедение других сущностей
    //Все сущности автоматически переводятся в CreatureType для удобства использования
    public Map<CreatureType, Double> getProbabilityEatCreatures(CreatureType creatureType) {
        Map<String, Object> rawConfigMap = getCreatureSetting(creatureType, "chanceToEat", Map.class);

        Map<CreatureType, Double> resultMap = new HashMap<>();
        for(String creatureKey : rawConfigMap.keySet()) {
            CreatureType currentCreatureType = CreatureType.getBySettingsPath(creatureKey);

            if(currentCreatureType == null) {
                throw new NullPointerException("The Creature with name ".concat(creatureKey)
                        .concat(" not found in chances to eat of ").concat(creatureType.name()));
            }

            resultMap.put(currentCreatureType, ((Number) rawConfigMap.get(creatureKey)).doubleValue() / 100);
        }

        return resultMap;
    }
}
