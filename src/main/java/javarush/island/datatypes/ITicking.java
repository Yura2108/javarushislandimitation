package javarush.island.datatypes;

public interface ITicking {

    /**
     * Method gives an opportunity to update the simulation items every tick
     */
    void doSimulationTick();

}
