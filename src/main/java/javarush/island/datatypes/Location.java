package javarush.island.datatypes;

import javarush.island.config.Configuration;
import javarush.island.items.Animal;
import javarush.island.items.BasicItem;
import javarush.island.items.Plant;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;


//Класс локации, тут хранится вся информация о локации, сущностях на ней, передвижениях между локациями
//Аннотация RequiredArgsConstructor создаёт конструктор для класса, только для финальных полей
@RequiredArgsConstructor
public class Location {

    //Ссылка на класс конфигурации, чтобы было удобнее брать настройки
    private final Configuration configuration;

    private final int xCoordinate;

    private final int yCoordinate;

    private final double creatureSpawnChance;

    //So that the cell is not overfilled
    //Делитель используется дальше в коде. КОНСТАНТА. Чтобы не создавать очень много животных
    private final int creaturesCountDivider = 5;

    //Список сущностей на текущей локации
    private final List<BasicItem> locationItems = new ArrayList<>();

    //Список всех сущностей, что на текущем тике локации решили передвинуться на другую или умерли
    //Используется как временное хранилище, поскольку при итерации основного списка удалять из него объекты нельзя
    private final List<BasicItem> listOfDeletedOrMoved = new ArrayList<>();

    //История такая же, как со списком выше, поскольку ничего нельзя добавлять в итерируемый список => используем временный список
    private final List<Animal> listOfCreatedChildren = new ArrayList<>();

    //Обращается в true, когда именно эта локация на очереди обработки
    private boolean inSimulationTick = false;


    //Метод инициализации локации
    public void initializeLocation() {
        //Fill location with plants
        growPlants();

        //Fill location with animals
        fillAnimals();
    }

    //Вызывается для обработки локации
    public void doSimulationTick() {
        inSimulationTick = true;

        //Проходимся по всем сущностям на локации, если они наследуют ITicking (имеют метод void doSimulationTick()) =>
        //Вызываем у них этот метод и идём дальше
        for(BasicItem basicItem : locationItems) {
            if(ITicking.class.isAssignableFrom(basicItem.getClass())) {
                ((ITicking) basicItem).doSimulationTick();
            }
        }

        //Проходимся по временным спискам, чистим их и модифицируем основной список
        locationItems.removeAll(listOfDeletedOrMoved);
        listOfDeletedOrMoved.clear();

        locationItems.addAll(listOfCreatedChildren);
        listOfCreatedChildren.clear();

        //Мы закончили обработку
        inSimulationTick = false;
    }

    //Пытаемся заполнить локацию растениями
    private void growPlants() {
        //Рандом - если не повезло, растений на локации не будет
        if(!ThreadLocalRandom.current().nextBoolean()) return;

        //Генерируем случайно, сколько КГ растительности будет на локации
        int randomPlantWeight = ThreadLocalRandom.current().nextInt(0,
                configuration.getCreatureSetting(CreatureType.PLANT, "maxNumberPerOneLocation", Number.class).intValue());

        //Если выпал 0 :(
        if(randomPlantWeight == 0) return;

        //Добавляем растение в список локаций.
        locationItems.add(new Plant(
                xCoordinate, yCoordinate, configuration.getCreatureSetting(CreatureType.PLANT, "emoji", String.class),
                randomPlantWeight
        ));
    }

    //Пытаемся заполнить локацию животными
    private void fillAnimals() {
        //Проходим по всем возможным типам сущностей
        for (CreatureType creatureType : CreatureType.values()) {
            //Если сущность не животное - скипаем
            if(!Animal.class.isAssignableFrom(creatureType.getItemClass())) continue;
            //Рандом - если не повезло, такого вида на локации не будет
            if(ThreadLocalRandom.current().nextDouble() > creatureSpawnChance) continue;

            //Получаем максимальное количество такой сущности на одной клетке + делим его, чтобы сократить популяцию
            //По итогу получаем максимальное количество для рандомного заполнения
            int maxQuantityInCell = configuration.getCreatureSetting(creatureType, "maxNumberPerOneLocation", Number.class)
                    .intValue() / creaturesCountDivider;

            //Получился 0 - не повезло
            if(maxQuantityInCell == 0) continue;
            //Генерируем случайное значение количества
            int animalsCount = ThreadLocalRandom.current().nextInt(0, maxQuantityInCell);
            if(animalsCount == 0) continue;

            //Создаём основной объект животного
            Animal newAnimal = Animal.createNewAnimal(creatureType, configuration, xCoordinate, yCoordinate);

            //Ошибка при создании?
            if(newAnimal == null) {
                System.err.println("Unknown error while creating new Animal " + this);

                continue;
            }

            //Добавляем в локацию клоны основного животного
            for (int i = 0; i < animalsCount; i++) locationItems.add(newAnimal.clone());
        }
    }

    //Метод для удаления с локации какого-то предмета
    public void removeItem(BasicItem basicItem) {
        //Если в симуляции сейчас обрабатывается эта локация - удалять из основного списка нельзя
        if(inSimulationTick) {
            listOfDeletedOrMoved.add(basicItem);
        }else {
            locationItems.remove(basicItem);
        }
    }

    //Аналогично методу с удалением, только добавление
    public boolean addNewBasicItem(BasicItem basicItem) {
        //Если количество данного вида превышает максимальное - возвращаем false
        if(getCountOfCreatures(basicItem.getCreatureType()) >= basicItem.getMaxQuantityInCell()) {
            return false;
        }

        if(inSimulationTick) {
            System.err.println("[LOG] An attempt to update location while location modifying");
            return false;
        }

        locationItems.add(basicItem);

        return true;
    }

    //Добавление ребёнка
    public void addChild(Animal animal) {
        listOfCreatedChildren.add(animal);
    }

    /**
     * Метод для получения количества указанного типа сущностей
     * @param creatureType Тип сущности
     * @return Количество указанного вида на локации
     */
    public long getCountOfCreatures(CreatureType creatureType) {
        return getLocationItems(null)
                .stream()
                .filter(basicItem -> basicItem.getCreatureType().equals(creatureType))
                .count();
    }

    /**
     * @param basicItem Remove BasicItem from list, if not null
     * @return Returns List of BasicItems on location, Excludes @basicItem parameter
     */
    public List<BasicItem> getLocationItems(BasicItem basicItem) {
        List<BasicItem> basicItems = new ArrayList<>(locationItems);
        basicItems.removeAll(listOfDeletedOrMoved);
        basicItems.addAll(listOfCreatedChildren);

        if(basicItem != null) basicItems.remove(basicItem);

        return basicItems;
    }

    /**
     * @return общее количество ЖИВОТНЫХ на локации
     */
    public long getAnimalsCount() {
        return getLocationItems(null)
                .stream()
                .filter(basicItem -> !basicItem.getCreatureType().equals(CreatureType.PLANT))
                .count();
    }

    //-- Для отладки --
    @Override
    public String toString() {
        return "Location{" +
                "xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                ", creatureSpawnChance=" + creatureSpawnChance +
                ", creaturesCountDivider=" + creaturesCountDivider +
                ", locationItems=" + locationItems.stream().map(BasicItem::getClass).collect(Collectors.toSet()) +
                '}';
    }
}
