package javarush.island;

import org.apache.commons.cli.*;

public class MainClass {

    //Это начало работы любой консольной программы (или точка входа). Основной метод, запускающий всю программу
    public static void main(String[] args) {
        //С помощью библиотеки CommonCLI от Apache мы инициализируем парсер консольных аргументов

        //Например: java -jar 123.jar -sf "test.yml"
        //В примере выше наглядное использование аргументов. Всё, что является аргументами, попадает в массив String[] args
        //Далее с помощью готовой библиотеки от Apache мы валидируем (проверяем правильность), выводим помощь в использовании
        //опций запуска

        // В данном случае опция одна - указание пути до файла настроек.
        Options options = new Options();

        //Описание опции
        Option settingsFileOption = new Option("sf", "settings_file", true, "settings file path");

        //Она не является обязательной, если файл настроек не указать - будет использован стандартный
        settingsFileOption.setRequired(false);

        //Добавляем опцию в общий список
        options.addOption(settingsFileOption);

        //Инициализируем нужные нам утилиты: помощь, парсер, результат
        HelpFormatter formatter = new HelpFormatter();
        CommandLineParser lineParser = new DefaultParser();
        CommandLine commandLine = null;

        try {
            commandLine = lineParser.parse(options, args);
        }catch (ParseException exception) {
            formatter.printHelp("settings_file", options);

            System.exit(1);
        }

        //Пытаемся получить значение опции, помним, что её могли не указать и будет null
        String settingsFilePath = commandLine.getOptionValue(settingsFileOption);

        //Если файл настроек не указан, запускаем симуляцию, предварительно уведомив, можно использовать другой файл настроек
        if(settingsFilePath == null) {
            System.out.println("Starting simulation with default settings...");

            System.out.println("You can modify this with command argument: ");
            formatter.printHelp("settings_file", options);
        }

        //Запуск симуляции
        Island.startSimulation(settingsFilePath);
    }


}
